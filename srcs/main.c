/*
** main.c for  in /home/broggi_t/projets/lem-in/srcs
** 
** Made by broggi_t
** Login   <broggi_t@epitech.eu>
** 
** Started on  Tue Apr 15 13:05:31 2014 broggi_t
** Last update Fri May  2 13:19:55 2014 a
*/

#include "lemin.h"

int		main(void)
{
  t_infos	infos;

  if (parser(&infos) == FAILURE)
    return (1);
  if (check_init(&infos) == FAILURE)
    return (1);
  algo(&infos);
  free_anthill(infos.first);
  return (0);
}
