/*
** get_the_direct.c for lem-in in /home/jacob_f/rendu/lemin/lem-in/srcs
** 
** Made by a
** Login   <jacob_f@epitech.net>
** 
** Started on  Fri May  2 16:28:09 2014 a
** Last update Sun May  4 15:08:54 2014 a
*/

#include <stdlib.h>
#include "lemin.h"

int		get_the_direct(t_infos *info,
			       t_int_list **int_list,
			       unsigned int *i)
{
  int		a;
  t_int_list	*new;

  a = 0;
  while (a < info->anthill->nb_pipe &&
	 info->anthill->pipes[a]->flag != END)
      a++;
  if (a == info->anthill->nb_pipe)
    return (FAILURE);
  if ((new = malloc(sizeof(t_int_list))) == NULL)
    return (my_perror(MALLOC_FAILED));
  new->next = NULL;
  new->prev = NULL;
  new->i = a;
  int_list[*i] = new;
  *i = *i + 1;
  int_list[*i] = NULL;
  return (SUCCESS);
}
