/*
** free_list.c for  in /home/broggi_t/minish
** 
** Made by broggi_t
** Login   <broggi_t@epitech.net>
** 
** Started on  Sun Jan  5 20:50:08 2014 broggi_t
** Last update Fri Mar  7 04:28:32 2014 broggi_t
*/

#include <stdlib.h>
#include "lemin.h"

void		free_anthill(t_anthill *list)
{
  t_anthill	*tmp;

  tmp = list;
  while (list)
    {
      list = list->next;
      free(tmp->name);
      if (tmp->to_malloc != -1)
	free(tmp->pipes);
      free(tmp);
      tmp = list;
    }
}

void		free_tmp_pipe(t_list_pipe *list)
{
  t_list_pipe	*tmp;

  tmp = list;
  while (list)
    {
      list = list->next;
      free(tmp);
      tmp = list;
    }
}
