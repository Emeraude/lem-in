/*
** algo.c for  in /home/broggi_t/projets/lem-in/srcs
** 
** Made by broggi_t
** Login   <broggi_t@epitech.eu>
** 
** Started on  Thu Apr 24 02:11:03 2014 broggi_t
** Last update Sun May  4 22:59:14 2014 a
*/

#include <stdlib.h>
#include "lemin.h"

int		algo(t_infos *infos)
{
  t_int_list	**tab;

  if ((tab = djikstra(infos)) == NULL)
    return (FAILURE);
  display_ant(infos, tab, 0);
  return (SUCCESS);
}
