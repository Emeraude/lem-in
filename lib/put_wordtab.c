/*
** put_wordtab.c for  in /home/broggi_t/projets/lem-in/lib
** 
** Made by broggi_t
** Login   <broggi_t@epitech.eu>
** 
** Started on  Sat Apr 26 15:34:39 2014 broggi_t
** Last update Sat Apr 26 15:34:39 2014 broggi_t
*/

#include "lemin.h"

void		put_wordtab(char **wordtab)
{
  register int	i;

  if (!wordtab)
    return ;
  i = -1;
  while (wordtab[++i])
    {
      my_putstr(wordtab[i], 1);
      my_putchar('\n', 1);
    }
}
