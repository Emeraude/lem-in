/*
** parser.c for  in /home/broggi_t/projets/minish2/srcs
** 
** Made by 
** Login   <broggi_t@epitech.net>
** 
** Started on  Tue Feb 25 12:50:40 2014 
** Last update Sun Mar  9 21:03:53 2014 
*/

int	my_isin(char *str, char c)
{
  int	i;

  i = -1;
  while (str[++i])
    if (str[i] == c)
      return (1);
  return (0);
}
