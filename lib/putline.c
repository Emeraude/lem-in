/*
** putline.c for  in /home/broggi_t/projet/lem-in/lib
** 
** Made by broggi_t
** Login   <broggi_t@epitech.eu>
** 
** Started on  Sun May  4 22:49:21 2014 broggi_t
** Last update Sun May  4 22:49:21 2014 broggi_t
*/

#include "lemin.h"

void	my_putline(char *str, const int fd)
{
  my_putstr(str, fd);
  my_putchar('\n', fd);
}
